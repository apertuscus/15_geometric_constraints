The paper presents an expression of motion constraints and a
JSON-schema language of expressing those constraints. These constraints
can be used to describe a task and monitor the progress online. The
method is applied to two robotic examples - opening a drawer and
sliding a tool on a surface.

Overall the topic is very important to robotics researchers. The
demonstrations show the potential of the methods. The paper is for the
most part clear and understandable. The video attachment clarifies the
demonstration examples. 

The main concern with the paper is that it does not make clear how this
method relates to prior work. Planning and control for constrained
behaviors is a well studied topic in robotics, and the paper does not
convince the reader that the constraint expression formalism is
significantly different from prior work or an improvement in some way.
Similarly the ideas of composing constraints or monitoring them online
are not new, and the paper does not explain how this work goes beyond
what has been shown before. The modeling language is an extension of
prior work. This reviewer believes that there likely is a new
contribution presented in the paper, however the paper does not
sufficiently place the contribution in the literature. Is it the case
that no previous method could be used to produce these experiments? Or
is there some way that this method is superior (better/faster/etc)?

In addition to the citation mentioned in the paper, Task Space Regions
come to mind as a similar constraint expression:
Berenson, D., Srinivasa, S. S., and Kuffner, J. "Task space regions: A
framework for pose-constrained manipulation planning." The
International Journal of Robotics Research, 2011.

The presentation of the constraint expression is very confusing. The
authors introduce a lot of vocabulary, such as entity, primitive,
expression, mechanisms, behavior, etc. Sections IV and V could be much
clearer if standard names were used and/or if a figure demonstrating
the concept was added.

How are the motions used in the examples generated? How are these
constraint expressions included in a planning or control scheme? There
is one paragraph at the end of section III does not say how the system
would work in non-trivial examples.

In addition I have a few more specific notes from various parts of the
paper:
* Section I, paragraph 1, "satisfying some geometric constraint" is not
what the robot has to do, grasping the handle or moving the object is
what the robot has to do, and satisfying a geometric constraint is one
way we might encode information about that task.
* Section I, first bullet, when would it not be possible for the
objective function to define the behavior? 
* Section I, first bullet, don't hyphenate an-d, instead the line break
can go after the /
* Section I, second bullet, the grammar here is wrong -- each bullet,
when combined with the lead-in phrase ("... in which" here) should form
a complete sentence (same comment on several other bulleted/numbered
lists)
* Section I, third bullet, does the instantaneous motion depend on the
switching, or just the overall motion?
* Section I, paragraph 3 (starting "With these ..."), the second
sentence should start "However, a large part of ..."
* Section I, second set of bullets, I do not understand what you mean
by separation of concerns
* Section II, first sentence, repeated "a"
* Section II, paragraph 1, list item ii, is this the same as the
"wrench basis", e.g. Murray, Li, Sastry, "A Mathematical Introduction
to
Robotic Manipulation", 1994, table 5.2?
* Section II, last paragraph, "This allows *us* to decouple..."
* Section III, paragraph 1, sentence "In [15]" should have a comma
after [15], no comma after "planning", and I don't understand what you
mean by "individuating infeasible solutions beforehand". Later on,
should be plural "unconstrained goals" (or "an unconstrained goal")
* Footnote 1, "all the models" should be plural 
* Section IV.A, the versor should have three degrees of freedom but is
encoded in four variables with a unit norm constraint (or you could
express the rotation in three variables but then you wouldn't have a
norm constraint)
* Check the IEEE style for tables -- they want the title on top for
some reason
* Section IV.B, first sentence is confusing
* Table II.a, the meaning of the horizontal subdividers is unclear and
they don't line up
* Section IV.E.c, "goal of the task is to constrain the end effector"
(no t on constrain)
* Section VI.A, what if there is a conflict within a level? Seems like
a strict ordering is needed
* Section VII, the ellipses are unclear here, do you mean
"uncertainties, etc., we..."?
* Section VII.B, the video has more than three states and uses
different names

Overall the paper seems like it might provide a nice contribution to
the literature, however the lack of clarity in the presentation and the
unclear relationship to past work is limiting the potential impact. 