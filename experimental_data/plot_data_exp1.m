clear all
close all
expdata2
l=L;
[r,c,v]=find(l>900);
for i=1:length(r)
    l(r(i),c(i))=NaN;
end

% 
% figure;
% for i=1:16
%     subplot(4,4,i)
% plot(l(:,D.time),l(:,i))
% legend(N(i))
% end



pos=[l(:,D.c_distance_x),l(:,D.c_distance_y),l(:,D.c_distance_z2)];

pos_norm=sqrt(sum(abs(pos').^2,1))';

line_point_dist=l(:,D.c_angle_approach);
gripper=l(:,D.open_right_gripper);
time=l(:,D.time);

ts=[  4.38,21.15,26.72, 38];
for i=1:numel(ts)
    i_ts(i)=find(time>ts(i),1);
end
%time interval of each task
ts1=1:i_ts(1);
ts2=i_ts(1)+1:i_ts(2);
ts3=i_ts(2)+1:i_ts(3);


fsizebig=[2285          53         499         702];


hfig1=figure();
 set(hfig1,'Position',fsizebig)
hold on;
subplot(3,1,1)
h=plot(time,[pos_norm,pos]);
set(h(1),'LineWidth',2)
%set(h(2),'LineStyle','-.')
set(h(3),'LineStyle','-.')
set(h(4),'LineStyle','-.')
grid on; box on;


hold on;
yvals=ylim();
deltay=yvals(2)-yvals(1);
ys=yvals(1)+deltay*0.15;
for i=1:4

plot([ts(i),ts(i)],[yvals(1),yvals(2)],'-.k')
if(i==1)
 text(ts(1)/2,ys,'S0','HorizontalAlignment','Center')
else
    text((ts(i)+ts(i-1))/2,ys,['S' num2str(i-1)],'HorizontalAlignment','Center')
end
end

legend('norm', 'x','y','z')
subplot(3,1,2)
h=plot(time,line_point_dist);
legend('Point-line dist.')

hold on;
yvals=ylim();
deltay=yvals(2)-yvals(1);
ys=yvals(1)+deltay*0.15;
for i=1:4

plot([ts(i),ts(i)],[yvals(1),yvals(2)],'-.k')
if(i==1)
 text(ts(1)/2,ys,'S0','HorizontalAlignment','Center')
else
    text((ts(i)+ts(i-1))/2,ys,['S' num2str(i-1)],'HorizontalAlignment','Center')
end
end

grid on; box on;
subplot(3,1,3)
h=plot(time,gripper);


hold on;
yvals=ylim();
deltay=yvals(2)-yvals(1);
ys=yvals(1)+deltay*0.15;
for i=1:4

plot([ts(i),ts(i)],[yvals(1),yvals(2)],'-.k')
if(i==1)
 text(ts(1)/2,ys,'S0','HorizontalAlignment','Center')
else
    text((ts(i)+ts(i-1))/2,ys,['S' num2str(i-1)],'HorizontalAlignment','Center')
end
end

xlabel('Time [s]')
legend('Gripper')
grid on; box on;

eval('print -dpsc2 ../image_answer/exp_data.eps')