
% before use, uncompress plot_tool and put in the matlab path
close all
expdata2

figsx=[D.exp_distance_tot
D.c_distance_x
D.c_distance_y
D.c_distance_z2];
D.exp_gripper;
fig_al=[
D.exp_align
D.exp_align_normals];
set(0, 'defaultTextInterpreter', 'none');
PRINT=true;


color_monitor=[1 1 1 ]*0.7;
w_monitor=8;
t_max=12;
i_max=find(L(:,D.time)==t_max);

indx=1:i_max;
ts=[  4.05,4.86,11.5];
for i=1:numel(ts)
    i_ts(i)=find(L(:,D.time)==ts(i));
end
%time interval of each task
ts1=1:i_ts(1);
ts2=i_ts(1)+1:i_ts(2);
ts3=i_ts(2)+1:i_ts(3);


fsizebig=[100 500 600 300];
fsizesmall=[100 500 600 200];

hfig1=figure();
 set(hfig1,'Position',fsizebig)
hold on;
%moniotor
h_mon=plot(L(ts1,D.time),L(ts1,D.exp_distance_tot),'Color',color_monitor,'LineWidth',w_monitor);
plot(L(ts3,D.time),L(ts3,D.exp_distance_x),'Color',color_monitor,'LineWidth',w_monitor)

h=plot(L(indx,D.time),L(indx,figsx));
set(h(1),'LineWidth',2)
%set(h(2),'LineStyle','-.')
set(h(3),'LineStyle','-.')
set(h(4),'LineStyle','-.')
legend([h; h_mon], {N{figsx} 'mon'})
grid on;box on
indxs=figsx;
ymin=min(min(L(indx,indxs)));
ymax=max(max(L(indx,indxs)));
Dlt=(ymax-ymin)/10;
ymin=ymin-Dlt;ymax=ymax+Dlt;
ylim([ymin,ymax])


yvals=ylim();
deltay=yvals(2)-yvals(1);
ys=yvals(1)+deltay*0.15;
for i=1:3

plot([ts(i),ts(i)],[yvals(1),yvals(2)],'-.k')
if(i==1)
 text(ts(1)/2,ys,'S1','HorizontalAlignment','Center')
else
    text((ts(i)+ts(i-1))/2,ys,['S' num2str(i)],'HorizontalAlignment','Center')
end
end

ylabel('Distance and positions [m]')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


hfig2=figure();
 set(hfig2,'Position',fsizesmall)

hold on;
h2=plot(L(indx,D.time),L(indx,fig_al));
set(h2(2),'LineStyle','--')
set(h2(2),'LineWidth',1)
legend( N{fig_al})
grid on;box on
indxs=fig_al;
ymin=min(min(L(indx,indxs)));
ymax=max(max(L(indx,indxs)));
Dlt=(ymax-ymin)/10;
ymin=ymin-Dlt;ymax=ymax+Dlt;
ylim([ymin,ymax])
yvals=ylim();
deltay=yvals(2)-yvals(1);
ys=yvals(1)+deltay*0.5;
for i=1:3

plot([ts(i),ts(i)],[yvals(1),yvals(2)],'-.k')
if(i==1)
 text(ts(1)/2,ys,'S1','HorizontalAlignment','Center')
else
    text((ts(i)+ts(i-1))/2,ys,['S' num2str(i)],'HorizontalAlignment','Center')
end
end
ylabel('Distance [m], angle [rad]')




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hfig3=figure();
 set(hfig3,'Position',fsizesmall)

 

hold on;
L(indx,D.exp_gripper)=L(indx,D.exp_gripper)*100;
h_mon=plot(L(ts2,D.time),L(ts2,D.exp_gripper),'Color',color_monitor,'LineWidth',w_monitor);
h2=plot(L(indx,D.time),L(indx,D.exp_gripper));
ylabel('Gripper opening [cm]')
grid on;box on;
indxs=D.exp_gripper;
ymin=min(min(L(indx,indxs)));
ymax=max(max(L(indx,indxs)));
Dlt=(ymax-ymin)/10;
ymin=ymin-Dlt;ymax=ymax+Dlt;
ylim([ymin,ymax])
yvals=ylim();
deltay=yvals(2)-yvals(1);
ys=yvals(1)+deltay*0.2;
for i=1:3

plot([ts(i),ts(i)],[yvals(1),yvals(2)],'-.k')
if(i==1)
 text(ts(1)/2,ys,'S1','HorizontalAlignment','Center')
else
    text((ts(i)+ts(i-1))/2,ys,['S' num2str(i)],'HorizontalAlignment','Center')
end
end
legend([h2; h_mon], {N{D.exp_gripper} 'mon'})

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(PRINT)
names={'dist.eps','align.eps', 'gripper.eps'};
h=[hfig1 hfig2 hfig3];
ystring=[];
for i=1:numel(h)
    set(0, 'CurrentFigure', h(i))
    xlabel('Time [s]')
    ystring=[ystring,label_y(1)];
    set(h(i),'PaperPositionMode','auto')
    
    eval(['print -dpsc2 ' names{i}]')
end
make_ps_frag_x(label_x)

make_ps_frag_y(ystring)


end
% hfig=figure();
% set(hfig,'Position',fsize) 
% 
% hold on;
% h2=plot(L(indx,D.time),L(indx,D.exp_head_align))
% legend( N{D.exp_head_align})
% grid on;box on
% 
% yvals=ylim()
% for i=1:3
% 
% plot([ts(i),ts(i)],[yvals(1),yvals(2)],'-.k')
% end


